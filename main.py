# -*- coding: utf-8 -*-
from pydal import DAL, Field
from pydal.helpers.classes import BasicStorage  # , XML, HTML
from selenium import webdriver
from selenium.webdriver.common.by import By
import selenium.webdriver.support.ui
import selenium.common.exceptions

import csv
import time
from datetime import date, datetime, timedelta
import os
import socket
import logging.config
import yaml
import smtplib
# from gdata.tlslite.TLSConnection import TLSConnection
# from gdata.tlslite.integration.ClientHelper import ClientHelper
import base64

from tiutools import send_mail

# INITS
HOME = os.path.expanduser("~")
F = "data/guest.csv"
GOWEB = True
BASEURL = "https://tiuapp.uvt.nl/idomeneo/!gastdb.gastdb."
SUPERUSER = BasicStorage(
    name="bmRlZ3Jvb3Q=",
    pw='b2FiaEcxNTIz',
)


def setup_logging(
        default_path='logging.yaml',
        default_level=logging.INFO,
        env_key='LOG_CFG'
):
    """ :returns logging object

    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.load(f.read(), Loader=yaml.FullLoader)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)
    return logging


# helper
def unpack_dicts(f):
    def f_new(*dicts):
        new_dict = {}
        for d in dicts:
            new_dict.update(d)
        return f(**new_dict)

    return f_new


@unpack_dicts
def unpack(memo, achternaam, voorletters, roepnaam, geboortedatum, straat,
           nummer, postcode, woonplaats, email, telefoonnummer, voorvoegsels="",
           toevoeging_nummer="", aanvulling=""):
    return (memo, achternaam, voorvoegsels, voorletters, roepnaam, geboortedatum, straat,
            nummer, toevoeging_nummer, aanvulling, postcode, woonplaats, email, telefoonnummer)


class Browser:
    """
    interact with (Chrome) browser logging in into the TiU guest db using SSO
    """

    def __init__(self, db):
        self.db = db
        self.base_url = BASEURL
        self.driver = None
        self.is_authenticated = False
        self.is_open = None

    def _open(self):
        if not self.driver:
            print("first open browser")
            self.driver = webdriver.Chrome()
            self._authenticate()
        else:
            self.is_open = True

    def get(self, command):
        self._open()  # open and authenticate
        url = self.base_url + command
        # print("command %s" % command)
        self.driver.get(url)
        return self.driver

    def click_submit_name(self, name):
        """
        find element with 'name'
        fix scrolling issue
        click it
        """
        submit = self.driver.find_element(By.NAME, name)
        if submit is None:
            print("Submit knop met naam: '" + name + "' niet gevonden")
            return False
        # fix
        self.driver.execute_script('window.scrollTo(0, ' + str(submit.location['y']) + ');')
        submit.click()

    def wait_for_name(self, name):
        wait = selenium.webdriver.support.ui.WebDriverWait(self.driver, 20)
        # timeout after 10 seconds
        return wait.until(lambda driver: driver.find_element(name))

    def _authenticate(self):
        """
        after opening authenticate SSO
        set flag is_authenticate if successful
        @return:
        """
        if self.is_authenticated:
            return
            # nothing to do
        user = SUPERUSER
        user.name = base64.b64decode(user.name).decode('ascii')
        user.pw = base64.b64decode(user.pw).decode('ascii')
        url = self.base_url + "main"
        self.driver.get(url)
        wait = selenium.webdriver.support.ui.WebDriverWait(self.driver, 20)
        # timeout after 10 seconds
        # wait for the button
        try:
            username = wait.until(lambda driver: driver.find_element(By.ID, 'username'))
            # was name, worked in FF
        except selenium.common.exceptions.TimeoutException:
            print("Exception in sso()")
        else:
            try:
                username.send_keys(user.name)
                self.driver.find_element(By.ID, 'password').send_keys(user.pw)
                self.click_submit_name('login')
            except selenium.common.exceptions.WebDriverException:  # in Chrome
                print("please give Chrome focus and type name and password ")
                time.sleep(20)
                # username.send_keys(base64.b64decode(user.name))
                # driver.find_element_by_id('password').send_keys(base64.b64decode(user.pw))

    def close(self):
        if self.driver:
            self.driver.close()
            self.is_open = False

    def create_letters(self):
        """
        generate the Word files
        """
        print("maak brieven")
        self.get("maak_wb")
        # wait for the button
        try:
            # check_all = \
            self.wait_for_name('CheckAll')
        except selenium.common.exceptions.ElementNotSelectableException:
            print("Exception in create_letters")
        else:
            try:
                self.click_submit_name('v_aktie')
                # above doesn't work using Firefox / webdriver
                # checkboxes = self.driver.find_elements("v_wb")
                # # visible = wait.until(lambda driver: checkboxes[0].is_displayed())
                # for checkbox in checkboxes:
                # checkbox.click()
                # submit = self.driver.find_element('v_aktie')
                # submit.click()
                # the Word docs are produced
            except selenium.common.exceptions.ElementNotSelectableException:
                print("Exception in create_letters: nothing to click??")
            except selenium.common.exceptions.NoSuchElementException:
                print("Exception in create_letters: Element not found")

    def download_letters(self):
        """
        Download the word docs
        @rtype : number of letters
        """
        opgehaald = []
        print("haal brieven")
        self.get("maak_wb")
        driver = self.driver
        wait = selenium.webdriver.support.ui.WebDriverWait(driver, 20)  # timeout after 10 seconds

        all_cb = self.driver.find_elements('v_wb')
        for ele in all_cb:
            ele.click()
        submit = self.driver.find_element('v_aktie')
        submit.click()

        # <a href="https://uvtapp.uvt.nl/idomeneo/!gastdb.gastdb.genereer_brief?
        # v_procs=maak_wb&amp;v_anr=275111">
        # <img border="0" alt="brief versturen" src="https://uvtapp.uvt.nl/images/uvt/word.gif"></a>
        while True:
            try:
                haal = wait.until(
                    lambda driver: driver.find_element_by_xpath("//a[contains(@href,"
                                                                "'genereer_brief')]/img"))
            except selenium.common.exceptions.TimeoutException:
                print("%i brieven opgehaald." % len(opgehaald))
                break
            else:
                href = driver.find_element_by_xpath("//a[contains(@href,'genereer_brief')]")
                anr = href.get_attribute('href').split('=')[2]
                # noinspection PyCallingNonCallable
                db(db.gast.anr == anr).update(status=3)
                db.commit()
                haal.click()  # updates the page
                opgehaald.append(anr)
                time.sleep(2)

        return opgehaald

    # noinspection PyUnboundLocalVariable
    def order_accounts(self):
        """
            order the accounts from a csv file by filling in
            a web form in the gastdb webapp , first checking if it exists in db.gast

        """
        db = self.db
        print(f"open file data/{F} as list of list")
        try:
            guests = list(csv.reader(open(F, 'r'), dialect=csv.excel))
        except FileNotFoundError:
            print(f"Expects file {F} with guest data to exists.")
            exit(2)

        enddate = datetime.now() + timedelta(days=365)
        einddatum = "%02i-%02i-%04i" % (enddate.day, enddate.month, enddate.year)

        if type(guests[0]) == list:
            # first line should be header, a list
            header = guests[0]
            fields = ("memo", "geslacht", "voorletters", "voorvoegsels", "achternaam",
                      "roepnaam", "straat", "nummer", "toevoeging_nummer", "aanvulling",
                      "postcode", "woonplaats", "land", "telefoonnummer",
                      "geboortedatum", "email")
            for (index, field) in enumerate(fields):
                if not header[index] == field:
                    print(f"header not ok at position {index} is {header[index]} should be {field}")
                    exit()
        if not GOWEB:
            exit()

        print("go to the gastdb form")
        self.get("invoer_gast")

        wait = selenium.webdriver.support.ui.WebDriverWait(self.driver, 20)
        # timeout after 20 seconds

        for user in guests:
            if type(guests[0]) == list:
                # it's a list try to unpack
                try:
                    (memo, geslacht, voorletters, voorvoegsels, achternaam, roepnaam, straat,
                     nummer, toevoeging_nummer, aanvulling,
                     postcode, woonplaats, land, telefoonnummer, geboortedatum,
                     email) = user
                except ValueError as e:
                    print(f"Overgeslagen {user}: Fout in csv, aantal velden klopt niet: {e}")
                    continue
                except Error as e:
                    print(f"Overgeslagen {user} Fout in csv: {e}")
                    continue
                if memo.lower() in ("studentnummer", "memo"):
                    continue  # skip the header
            elif type(guests[0]) == dict:
                (memo, voorletters, voorvoegsels, achternaam, roepnaam, straat,
                 nummer, toevoeging_nummer, aanvulling, postcode, woonplaats, land,
                 telefoonnummer, geboortedatum,
                 email) = unpack(user)
            else:
                print("Datastructuur niet geaccepteerd, gebruik a list of lists OF a list of dicts")
                break

            if geslacht not in 'MV':
                print(f"Veld 'geslacht' kan alleen M of V zijn is nu {geslacht}")
                break
            if not postcode:
                print("Veld 'postcode' should be present")
                break

            for sep in ('-', '.'):
                if sep in geboortedatum:
                    break
            else:
                print("Veld 'geboortedatum' ,moet als scheidingsteken '-' of '.' hebben.")
                break
            (geb_dag, geb_maand, geb_jaar) = geboortedatum.strip().split(sep)
            if 4 == len(geb_dag):  # jaar-maand-dag
                (geb_jaar, geb_maand, geb_dag) = geboortedatum.strip().split(sep)

            (geb_dag, geb_maand) = (str(int(geb_dag)), str(int(geb_maand)))
            # already ordered or existing ?
            searchname = achternaam + ", " + voorletters
            if len(voorvoegsels) > 0:
                searchname += (" " + voorvoegsels)
            account = db(db.gast.naam == searchname).select(db.gast.ALL)
            if account:
                print(f"Er is al een account voor {searchname} nl {account[0].anr}")
                continue
            # lets run
            an = wait.until(lambda d: d.find_element(By.NAME, 'v_achternaam'))
            an.send_keys(achternaam)
            self.driver.find_element(By.NAME, 'v_tussenvoegsel').send_keys(voorvoegsels)
            self.driver.find_element(By.NAME, 'v_voorletters').send_keys(voorletters)
            self.driver.find_element(By.NAME, 'v_roepnaam').send_keys(roepnaam)
            self.driver.find_element(By.NAME, 'v_geslacht').send_keys(geslacht)

            jaar = wait.until(lambda driver: driver.find_element(By.NAME, 'v_jaar'))
            selenium.webdriver.support.ui.Select(jaar).select_by_visible_text(geb_jaar)
            maand = wait.until(lambda driver: driver.find_element(By.NAME, 'v_maand'))
            selenium.webdriver.support.ui.Select(maand).select_by_visible_text(geb_maand)
            dag = wait.until(lambda driver: driver.find_element(By.NAME, 'v_dag'))
            selenium.webdriver.support.ui.Select(dag).select_by_visible_text(geb_dag)

            self.driver.find_element(By.NAME, 'v_comm_taal').send_keys("N")
            self.driver.find_element(By.NAME, 'v_email').send_keys(email)
            self.driver.find_element(By.NAME, 'v_org_afk').send_keys("T")

            ed = self.driver.find_element(By.NAME, 'v_einddatum')
            ed.clear()
            ed.send_keys(einddatum)
            self.driver.find_element(By.NAME, 'v_toegang_bb').click()
            self.driver.find_element(By.NAME, 'v_tiu_email').click()
            self.driver.find_element(By.NAME, 'v_straat').send_keys(straat)
            self.driver.find_element(By.NAME, 'v_huisnummer').send_keys(nummer)
            self.driver.find_element(By.NAME, 'v_huizen_toev').send_keys(toevoeging_nummer +
                                                                         ' '
                                                                         + aanvulling)
            self.driver.find_element(By.NAME, 'v_postcode').send_keys(postcode)
            self.driver.find_element(By.NAME, 'v_plaats').send_keys(woonplaats)
            lb_land = selenium.webdriver.support.ui.Select(self.driver.find_element(By.NAME,
                                                                                    "v_land"))
            lb_land.select_by_visible_text(land if land else 'Netherlands')
            self.driver.find_element(By.NAME, 'v_telnummer').send_keys(telefoonnummer)
            self.driver.find_element(By.NAME, 'v_opm').send_keys(memo)
            submit = wait.until(lambda driver: driver.find_element(By.NAME, 'v_aktie'))

            # fix element-has-moved problem
            self.driver.execute_script('window.scrollTo(0, ' + str(submit.location['y']) + ');')

            submit.click()

    def update_database(self):

        """
        Using the overview page on the TiU gastdb website read the list of
        accounts en put anr, name, org, start and end date in the local database
        @return: # of updated records
        """
        db = self.db
        print('open zoekpagina')
        self.get('zoeken_gast')
        driver = self.driver
        wait = selenium.webdriver.support.ui.WebDriverWait(driver, 25)  # timeout after X seconds
        try:
            def find_element(driver):
                return driver.find_element(By.NAME, 'v_org_afk')

            org = wait.until(find_element)
        except selenium.common.exceptions.TimeoutException:
            print("Zoekpagina werd niet geopend!")
            exit()
        else:
            org.send_keys('T')

        print('zoekpagina is ingevuld')
        css_sel = "input[value='zoek']"
        submit = wait.until(lambda driver: driver.find_element(By.CSS_SELECTOR, css_sel))
        # fix
        driver.execute_script('window.scrollTo(0, ' + str(submit.location['y']) + ');')

        submit.click()
        # ga naar de overzichtspagina met de gebruikers
        data = []
        print("processing.", end="")
        for tr in driver.find_elements(By.XPATH, '//*[@id="c2"]//tr'):
            tds = tr.find_elements(By.TAG_NAME, 'td')
            if tds:
                print(".", end="")
                data.append([td.text for td in tds])
        print()

        def mkdate(d_str):
            return datetime.strptime(d_str, '%d-%b-%y')

        nr_new = 0
        for index, row in enumerate(data):
            if index == 0:  # header
                continue
            inserted_id = db.gast.update_or_insert(db.gast.anr == row[2],
                                                   anr=row[2],
                                                   naam=row[3],
                                                   organisatie=row[4],
                                                   startdatum=mkdate(row[5]),
                                                   einddatum=mkdate(row[6]))
            if inserted_id:
                # new guest added
                nr_new += 1
                db(db.gast.id == inserted_id).update(status=2)
            db.commit()
        return len(data), nr_new

    def update_account_expiration(self, account):

        """
        Using the search page on the TiU gastdb find and update account
        also in db
        :param account:
        :return: # success (bool)
        """
        db = self.db
        new_exp_date = date.today() + timedelta(days=365)
        new_exp_date_str = "%02i-%02i-%04i" % (new_exp_date.day,
                                               new_exp_date.month,
                                               new_exp_date.year)
        new_exp_date_db = "%04i-%02i-%02i" % (new_exp_date.year,
                                              new_exp_date.month,
                                              new_exp_date.day)

        print('open zoekpagina')
        self.get('zoeken_gast')
        driver = self.driver
        wait = selenium.webdriver.support.ui.WebDriverWait(driver, 25)  # timeout after X seconds
        try:
            def find_element(driver):
                return driver.find_element('v_anr')

            field_anr = wait.until(find_element)
        except selenium.common.exceptions.TimeoutException:
            print("Zoekpagina werd niet geopend!")
            exit()
        else:
            field_anr.send_keys(account.anr)

        print('zoekpagina is ingevuld')
        css_sel = "input[value='zoek']"
        submit = wait.until(lambda driver: driver.find_element_by_css_selector(css_sel))
        # fix possible scroll, making [submit] visible
        driver.execute_script('window.scrollTo(0, ' + str(submit.location['y']) + ');')

        submit.click()
        # wait for the result page with the users, should be only one
        data = []
        links = []
        print("processing.", end="")
        for tr in driver.find_elements(By.XPATH, '//*[@id="c2"]//tr'):
            tds = tr.find_elements(By.TAG_NAME, 'td')
            if tds:
                print(".", end="")
                data.append([td.text for td in tds])

            links.append([link.get_attribute('href') for link in tr.find_elements(By.TAG_NAME,
                                                                                  'a')])

        print()

        def update_expiration_form(edit_page):
            self.driver.get(edit_page)

            input_ed = self.driver.find_element(By.NAME, 'v_einddatum')
            input_ed.clear()
            input_ed.send_keys(new_exp_date_str)
            submit = wait.until(lambda driver: driver.find_element(By.TAG_NAME, 'v_aktie'))

            # fix element-has-moved problem
            self.driver.execute_script('window.scrollTo(0, ' + str(submit.location['y']) + ');')

            submit.click()
            # alert 'are you sure?' YES!
            alert_obj = self.driver.switch_to.alert
            alert_obj.accept()

        update_expiration_form(links[1][0])
        db(db.gast.anr == account.anr).update(einddatum=new_exp_date_db)
        db.commit()

    def daily_update_database(self, force=False):
        """
        update the database max daily, remember the date in config table
        :param force: if true update at every run
        """
        db = self.db
        yesterday = db(db.config.name == "last_run").select(db.config.value)
        if not yesterday:  # not in db: first run
            yesterday = date.today() - timedelta(days=1)
            # pretent it was yesterday, that's long enough ago
        else:
            yesterday = datetime.strptime(yesterday[0].value, "%Y-%m-%d").date()
        if force or (date.today() - yesterday).days > 0:  # yesterday or longer ago
            total, new = self.update_database()
            db.config.update_or_insert(db.config.name == "last_run",
                                       name="last_run",
                                       value=str(date.today()))
            db.commit()
            print("%i kaarten geüpdatet %i toegevoegd" % (total, new))

    def update_expirations_group(self, group=None):
        # type: (basestring) -> object
        # select guest accounts with group=group
        # adjust expiry to today next year - 1
        if not group:
            print('I need a group as parameter')
            return

        accounts = db(db.gast.groep == group).select(db.gast.ALL)
        if len(accounts) == 0:
            print("no accounts found in db with group '{}'".format(group))
        for account in accounts:
            self.update_account_expiration(account)


class LocalDAL(DAL):
    """ use local (synced on Surfdrive) sqlite database 'gastdb' """

    def __init__(self):
        super(LocalDAL, self).__init__('sqlite://guest.sqlite',
                                       folder=os.path.join(HOME,
                                                           'SURFdrive',
                                                           'databases',
                                                           'gastdb'),
                                       migrate=True,
                                       migrate_enabled=True,
                                       fake_migrate_all=False)

        self.define_table('config',
                          Field('name', 'string', unique=True),
                          Field('value', 'string'),
                          Field('memo', 'string'))

        self.define_table('status',
                          Field('name', 'string', unique=True))

        self.define_table('aanvraag',
                          Field('email', 'string', unique=True),
                          Field('memo', 'string'),
                          Field('achternaam', 'string'),
                          Field('voorvoegsels', 'string'),
                          Field('voorletters', 'string'),
                          Field('roepnaam', 'string'),
                          Field('geboortedatum', 'string'),
                          Field('straat', 'string'),
                          Field('nummer', 'string'),
                          Field('toevoeging_nummer', 'string'),
                          Field('aanvulling', 'string'),
                          Field('postcode', 'string'),
                          Field('woonplaats', 'string'),
                          Field('land', 'string'),
                          Field('telefoonnummer', 'string'),
                          Field('status', 'integer'),
                          )

        self.define_table('gast',
                          Field('anr', 'string', unique=True),
                          Field('naam', 'string'),
                          Field('organisatie', 'string'),
                          Field('startdatum', 'date'),
                          Field('einddatum', 'date'),
                          Field('groep', 'string'),
                          Field('status', 'integer'),
                          )

    def create_bb_enroll(self, courseid):
        """ create an enroll file with name 'courseid' to be imported in Blackboard """
        import csv

        db = self
        try:
            ofile = open(courseid + '.csv', 'wb')
            writer = csv.writer(ofile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
            # noinspection PyCallingNonCallable
            db_rows = db(db.gast.status == 3).select(db.gast.anr)
            for student in db_rows:
                row = (courseid, "U" + student.anr, "S")
                writer.writerow(row)
            ofile.close()
        except IOError as e:
            print
            "BB file I/O error({0}): {1}".format(e.errno, e.strerror)
        except Exception as e:
            print("bb file not made because of error {}".format(e))
        else:
            print("bb file created")
            # noinspection PyCallingNonCallable
            db(db.gast.status == 3).update(status=4)
        return


if __name__ == "__main__":
    db = LocalDAL()
    browser = Browser(db)
    setup_logging()

    # noinspection PyTypeChecker
    browser.daily_update_database(force=False)  # Force:True every run
    # browser.update_expirations_group(group='ksvg2')
    # browser.show_logins_group(group='spir_sam')
    browser.order_accounts()  # also checks database for accounts already created
    # browser.create_letters()
    # browser.download_letters()
    # db.create_bb_enroll("n-TST-011-2014-2015")  #

    browser.close()
    # test_mail()
