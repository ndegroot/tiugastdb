import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os

print("Test 'normal' Chrome")
driver = webdriver.Chrome()
print("%s" % driver)
driver.get("http://www.google.com")
time.sleep(12)
driver.close()

print("Test 'headless' Chrome, check for capture.png")
# test headless
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--window-size=1920x1080")

driver = webdriver.Chrome(chrome_options=chrome_options)  # , executable_path=chrome_driver)
driver.get("https://www.google.com")
lucky_button = driver.find_element_by_css_selector("[name=btnI]")
lucky_button.click()

# capture the screen
driver.get_screenshot_as_file("capture.png")

#  returns on Chrome V.30
#  returns on Chrome V.67
